<?php
/**
 * Created by PhpStorm.
 * User: TusharSuvro
 * Date: 1/22/2017
 * Time: 5:42 PM
 */

namespace App;


class Student
{


    private $name;
    private $studentID;

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setStudentID($studentID)
    {
        $this->studentID = $studentID;
    }

    public function getStudentID()
    {
        return $this->studentID;
    }

}